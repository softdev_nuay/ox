import java.util.Scanner;
public class OX {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        String[][] tarang = { { "-", "-", "-" }, { "-", "-", "-" }, { "-", "-", "-" } };
        for (int i = 1; i <= 3; i++) {
            for (int j = 1; j <= 3; j++) {
                System.out.print(tarang[i - 1][j - 1]);
            }
            System.out.println();
        }
        String turn = "X";
        String player = "1";
        for (int k = 0; k < tarang.length * tarang.length ; k++) {
            System.out.print("Player " + player + " Turn : " + turn);
            //String move = scan.nextLine();
            System.out.print(" row = ");
            int row = scan.nextInt();
            System.out.print(" column = ");
            int col = scan.nextInt();
            if (row > tarang.length || col > tarang.length){
                continue;
            } else if (tarang[row][col] != "-") {
                continue;
            } else {
                tarang[row][col] = turn;
                if (turn == "X") {
                    turn = "O";
                } else {
                    turn = "X";
                }
                if (player == "1") {
                    player = "2";
                } else {
                    player = "1";
                }
            }
            for (int i = 1; i <= 3; i++) {
                for (int j = 1; j <= 3; j++) {
                    System.out.print(tarang[i - 1][j - 1]);
                }
                    System.out.println(); 
            }
        }
        scan.close();
    }
}